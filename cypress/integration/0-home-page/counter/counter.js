import { Then } from "cypress-cucumber-preprocessor/steps";

Then(`I should be able to see {string} heading`, (message) => {
  cy.get("h1").contains(message);
});

Then(`I should be able to see {string} paragraph`, (message) => {
  cy.contains("p", message);
});
