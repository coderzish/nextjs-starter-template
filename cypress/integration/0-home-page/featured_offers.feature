Feature: Home

  Home page

  Scenario: Should be able to see featured offers

    Given I have the following featured offers on the server with total count 100
      | Id     |
      | 408475 |
      | 408515 |
    Given I open home page

# Then I should see the featured offers

