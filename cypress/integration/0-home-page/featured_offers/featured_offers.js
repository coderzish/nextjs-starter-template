const { Given } = require("cypress-cucumber-preprocessor/steps");
const { getMerchants } = require("../../common/data/merchants");
const { getOffers } = require("../../common/data/offers");

Given(
  `I have the following featured offers on the server with total count 100`,
  (data) => {
    const offers = getOffers();
    const merchants = getMerchants();
    console.log("data", data.hashes());
    const resultData = (data.hashes() || []).map((item) => {
      const offer = offers.find((offer) => offer.Id == item.Id);
      const merchant = merchants.find(
        (merchant) => merchant.Id == offer.MerchantId
      );
      return { ...offer, Merchant: merchant };
    });
    cy.intercept(
      "https://api-uat.cashrewards.com.au/api/v1/offer/featured-offers",
      {
        Count: 100,
        Data: resultData,
        TotalCount: 100,
      }
    );
  }
);
