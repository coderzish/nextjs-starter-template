Feature: Home

  Home page

  Scenario: Should be able to see welcome message

    Given I go to "home" page
    Then I should be able to see "Welcome Home" heading
    Then I should be able to see "Click me" button
    When I click on "Click me" button
    Then I should be able to see "You clicked me 1 times. Keep clicking." paragraph
    When I click on "Click me" button
    Then I should be able to see "You clicked me 2 times. Keep clicking." paragraph


# Then I should see the featured offers

