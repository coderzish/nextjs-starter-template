import { Then } from "cypress-cucumber-preprocessor/steps";
/**
 * https://github.com/cucumber/cucumber-expressions#parameter-types
 */
Then(`I should be able to see {string} button`, (content) => {
  cy.get("button").contains(content);
});

When(`I click on {string} button`, (buttonContent) => {
  cy.contains("button", buttonContent).click();
});
