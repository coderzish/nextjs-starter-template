import { Before, Given } from "cypress-cucumber-preprocessor/steps";

const pagesMap = {
  home: "/",
};

Given(`I go to {string} page`, (page) => {
  cy.visit(pagesMap[page]);
});

Before(() => {
  cy.on("uncaught:exception", () => {
    return false;
  });
});
