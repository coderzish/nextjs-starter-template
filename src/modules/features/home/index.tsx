import Link from "@/components/Link";
import { Button, Container, Typography } from "@mui/material";
import Head from "next/head";
import React, { useState } from "react";
import styled from "styled-components";

const MainStyled = styled("div")`
  padding: 5rem 0;
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export default function HomePage() {
  const [clickCounter, setClickCounter] = useState(0);

  return (
    <Container>
      <Head>
        <title>Home page</title>
        <meta name="description" content="Issue tracking app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <MainStyled>
        <Typography variant="h1">Welcome Home</Typography>
        <Button
          variant="contained"
          onClick={() => setClickCounter(clickCounter + 1)}
        >
          Click me
        </Button>
        {clickCounter > 0 && (
          <Typography>
            You clicked me {clickCounter} times. Keep clicking.
          </Typography>
        )}
        <br />
        <Link href="/issues/list">Issue list</Link>
      </MainStyled>
    </Container>
  );
}
