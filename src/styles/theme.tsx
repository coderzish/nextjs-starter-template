import { createTheme } from "@mui/material/styles";

export const theme = createTheme({
  palette: {
    primary: {
      main: "#8700d7",
    },
    secondary: {
      main: "#BB68D4",
    },
    background: {
      default: "#FFFFFF",
    },
  },
  typography: {
    fontFamily: '"Montserrat", sans-serif;',
    h1: {
      fontWeight: 900,
      fontSize: 28,
      color: "#282828",
    },
  },

  components: {
    MuiButtonBase: {
      defaultProps: {
        disableRipple: true,
        disableTouchRipple: true,
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          textTransform: "none",
        },
      },
    },
    MuiFormLabel: {
      styleOverrides: {
        root: {
          color: "#666666",
          fontSize: 16,
          fontWeight: 500,
          lineHeight: "130%",
          paddingBottom: 4,
        },
      },
    },
    MuiInputBase: {
      styleOverrides: {
        root: {
          //hide up/down arrow for number input fields
          "& input::-webkit-clear-button, & input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button":
            {
              display: "none",
              margin: 0,
            },
        },
      },
    },
    MuiFormHelperText: {
      styleOverrides: {
        root: {
          fontSize: 14,
          lineHeight: "130%",
          letterSpacing: "0.01em",
          "&$error": {
            color: "#EB001B",
          },
        },
      },
    },
    MuiIconButton: {
      styleOverrides: {
        root: {
          "&:hover": {
            backgroundColor: "transparent",
          },
        },
      },
      defaultProps: {
        disableRipple: true,
        disableTouchRipple: true,
        disableFocusRipple: true,
      },
    },
  },
});
