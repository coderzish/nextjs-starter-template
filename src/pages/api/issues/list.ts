import type { NextApiRequest, NextApiResponse } from "next";
export default function handler(_: NextApiRequest, res: NextApiResponse): void {
  const status = 100;
  res.status(status).json({ name: "John Doe" });
}
