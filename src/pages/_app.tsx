import { AppProps } from "next/app";
// import "../styles/globals.css";
import Head from "next/head";
import React from "react";
import { ThemeProvider } from "@mui/material/styles";
import { theme } from "@/styles/theme";
// import { ThemeProvider } from "@mui/material/styles";
import { UserProvider } from "@auth0/nextjs-auth0";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <meta
          content="minimum-scale=1, initial-scale=1, width-device-width"
          name="viewport"
        />
        <title>Title of the app</title>
      </Head>
      <ThemeProvider theme={theme}>
        <div>
          <UserProvider>
            <Component {...pageProps} />
          </UserProvider>
        </div>
      </ThemeProvider>
    </>
  );
}

export default MyApp;
