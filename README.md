# Next js boilerplate code

# Local development

## Prerequisite

1. Node version ^14.17.1
2. Yarn version ^1.22.11
3. Install dependencies using yarn

```bash
yarn
```

## Development

1. Start the development server

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Development environment setup

1. Copy settings.example.json to your local .vscode folder

```bash
cp settings.example.json .vscode/settings.json
```

2. Copy .env.example file to .env.local

```bash
cp .env.example .env.local

```

### VSCode extension recommendation

1. dbaeumer.vscode-eslint
2. esbenp.prettier-vscode
3. rvest.vs-code-prettier-eslint
4. alexkrechik.cucumberautocomplete
5. andrew-codes.cypress-snippets
6. jpoissonnier.vscode-styled-components

## Folder structure

<img src="docs/assets/images/src_folder_structure_1.png" style="height: 500px"/>

### components

This folder will contain all the UI components

### modules

This folder will contain all the modules and feature related containers and services

### pages

This folder will contain all the pages and APIs only.

### styles

This folder will contain theme related global styles and style dictionary.

The
[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/issues/list](http://localhost:3000/api/issues/list). This endpoint can be edited in `pages/api/issues/list.ts`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

# Testing

## E2E testing

This repository uses [cypress](https://docs.cypress.io/) and cucumber for E2E testing. See [https://github.com/TheBrainFamily/cypress-cucumber-preprocessor](https://github.com/TheBrainFamily/cypress-cucumber-preprocessor) for more information.

Cucumber allows us to define tests using [Gherkin](https://cucumber.io/docs/gherkin/reference/) syntax

### Running test

```bash
yarn test:integration
```

## Folder structure

![folder structure](/docs/assets/images/cypress_folder_structure.png "folder structure")

### fixtures

This folder contains all test data.

### integration

This folder contains all feature(spec) files. Each folder structure within this will be based on features. Each feature folder can have sub feature folders. Each feature should have a folder and Javascript file that defines the implementation for each Gherkin syntax.

For e.g The counter.feature file defines feature as

<img src="docs/assets/images/gherkin_feature_syntax_sample_1.PNG" style="width: 600px"/>

The counter.js file provides the corresponding implementation for some of those syntax. For e.g.

<img src="docs/assets/images/gherkin_feature_implementation_sample_1.PNG" style="width: 600px"/>.

The common implementation can go to common directory. For e.g

<img src="docs/assets/images/page_js_sample_code.PNG" style="width: 600px"/>.

See [Cucumber expression](https://github.com/cucumber/cucumber-expressions#introduction) for more on how to use variables on the expressions.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!
